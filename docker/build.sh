#!/usr/bin/env bash

# Arguments :
#  docker image path : Path to the docker image (ex: "path/to/image")

# Color and styles
NORMAL=$'\e[0m'
BLUE=$'\e[36m'
RED=$'\e[31;01m'
YELLOW=$'\e[33m'
SUCCESS=$'\e[1m\e[42m\e[30m'
ERROR=$'\e[1m\e[41m\e[30m'

# Check arguments
if [ "$#" -ne 1 ]; then
    echo ""
    echo "${RED}Invalid arguments!${NORMAL}"
    echo ""
    echo "${BLUE}Please give the path to the docker image."
    echo "${BLUE}Example: ${NORMAL}./docker/build.sh path/to/image${BLUE}."
    exit 1
fi

TARGET_IMAGE="base:latest"
DOCKERFILE="./docker/images/base/Dockerfile"
DOCKER_IMAGE_PATH="$1"

# Build
echo "${YELLOW} + Building target ${NORMAL}${TARGET_IMAGE}"
echo ""
echo "  $ docker build --no-cache -t ${DOCKER_IMAGE_PATH}/${TARGET_IMAGE} -f ${DOCKERFILE} .${NORMAL}"
docker build --no-cache -t "${DOCKER_IMAGE_PATH}"/${TARGET_IMAGE} -f ${DOCKERFILE} .
RESULT=$?
echo ""
if [[ ${RESULT} -eq 0 ]]; then
    echo "${SUCCESS} Build successful ${NORMAL}"
else
    echo "${ERROR} Build failed ${NORMAL}"
    echo ""
    exit ${RESULT}
fi
