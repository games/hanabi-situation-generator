/**
 * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_Expressions
 */
const escapeRegExp = (string) => {
  return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched string
};

const buildRegExpColorList = (availableColors = [], acceptRandom = true) => {
  return [...availableColors, ...(acceptRandom ? ['X'] : [])]
    .map(escapeRegExp)
    .join('|');
};
const buildRegExpValueList = (acceptRandom = true) => {
  const valueList = [...(new Array(5))].map((value, index) => index + 1);
  return [...valueList, ...(acceptRandom ? ['X'] : [])].join('|');
};

exports.buildRegExpColorList = buildRegExpColorList;
exports.buildRegExpValueList = buildRegExpValueList;
