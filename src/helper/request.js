const PROTOCOLE = process.env.PROTOCOLE || 'http';
const PORT = process.env.PORT || 4200;
const HIDE_PORT = process.env.HIDE_PORT || false;

const getBaseUrlFromRequest = (request) => {
  return `${PROTOCOLE}://${request.hostname}${!HIDE_PORT ? ':' + PORT : ''}`;
};

const getQueryParam = (req, paramName, defaultValue = null) => {
  const paramValue = req.query[paramName];
  if (!paramValue || paramValue.length === 0) {
    return defaultValue;
  }

  return decodeURIComponent(paramValue);
};

const buildQueryString = (queryParams) => {
  return '?' + (queryParams
    .map(([paramName, paramValue]) => {
      return `${paramName}=${encodeURIComponent(paramValue)}`;
    })
    .join('&'));
};

const parseStringList = (stringList) => {
  if (stringList.trim().length === 0) {
    return [];
  }

  return stringList
    .split(',')
    .map((value) => value.trim());
};

const encodeToStringList = (list) => {
  return list.join(',');
};

const parseMultipleStringLists = (multipleStringLists) => {
  if (multipleStringLists.trim().length === 0) {
    return [];
  }

  return multipleStringLists
    .split('|');
};

const encodeToMultipleStringLists = (list) => {
  return list
    .map(encodeToStringList)
    .join('|');
};

const parsePlayerHandStrings = (stringList) => {
  return parseMultipleStringLists(stringList).map((playerHand) => {
    const parts = playerHand
      .replaceAll(' ', '')
      .split('<-');

    const cards = parts[0]
      .split(',')
      .map((value) => value.trim());

    if (parts.length < 2) {
      return cards;
    }

    cards.push('<-' + parts[1].trim());

    return cards;
  });
};


exports.getBaseUrlFromRequest = getBaseUrlFromRequest;
exports.getQueryParam = getQueryParam;
exports.buildQueryString = buildQueryString;
exports.parseStringList = parseStringList;
exports.encodeToStringList = encodeToStringList;
exports.parseMultipleStringLists = parseMultipleStringLists;
exports.encodeToMultipleStringLists = encodeToMultipleStringLists;
exports.parsePlayerHandStrings = parsePlayerHandStrings;
