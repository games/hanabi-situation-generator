const { CONFIG } = require('../config');
const { getParser } = require('./cardParser');
const { getRandomItem } = require('../helper/collection');

const parse = getParser();

describe('parseCard()', () => {
  describe('Invalid', () => {
    test('Blank strings should be null card', () => {
      expect(parse('')).toBe(null);
      expect(parse('    ')).toBe(null);
    });

    test('<InvalidInitial>X should return null', () => {
      expect(parse('DX')).toBe(null);
    });

    test('<InvalidCaseInitial>X should return null', () => {
      expect(parse(CONFIG.AVAILABLE_COLORS[0].toLowerCase() + 'X')).toBe(null);
    });

    test('X<InvalidValue> should return null', () => {
      expect(parse('X0')).toBe(null);
      expect(parse('X99')).toBe(null);
      expect(parse('XD')).toBe(null);
    });
  });

  describe('Colors', () => {
    CONFIG.AVAILABLE_COLORS.forEach((item) => {
      test(`"${item}X" card`, () => {
        expect(parse(`${item}X`)).toMatchObject({
          color: { known: item, real: item },
        });
      });
    });
  });

  describe('Values', () => {
    CONFIG.AVAILABLE_VALUES.forEach((item) => {
      test(`"X${item}" card`, () => {
        expect(parse(`X${item}`)).toMatchObject({
          value: { known: item, real: item },
        });
      });
    });
  });

  describe('Random', () => {
    test('XX should be a known random card', () => {
      expect(parse('XX')).toMatchObject({
        color: { known: null, real: null },
        value: { known: null, real: null },
      });
    });
  });

  describe('Unknown', () => {
    test('(X)(X) should be an unknown random card', () => {
      expect(parse('(X)(X)')).toMatchObject({
        color: { known: null, real: null },
        value: { known: null, real: null },
      });
    });
  });

  describe('Combination', () => {
    const color = getRandomItem(CONFIG.AVAILABLE_COLORS);
    const color2 = getRandomItem(CONFIG.AVAILABLE_COLORS.filter((item) => item !== color));
    const value = getRandomItem(CONFIG.AVAILABLE_VALUES);
    const value2 = getRandomItem(CONFIG.AVAILABLE_VALUES.filter((item) => item !== value));

    // A card can be
    const given = [
      // known
      `${color}${value}`,
      `${color}X`,
      `X${value}`,
      // partially known
      `${color}(${value})`,
      `X(${value})`,
      `(${color})${value}`,
      `(${color})X`,
      `(${color})(${value})`,
      `(${color})(X)`,
      `(X)(${value})`,
      `(${color}${value})`,
      `(${color}X)`,
      `(X${value})`,
      // unknown
      `XX`,
      `X(X)`,
      `(X)X`,
      `(X)(X)`,
      `(XX)`,
      // with a known color different from the real one
      `${color}(${color2})${value}`,
      `${color}(${color2})X`,
      `${color}(${color2})(${value})`,
      `${color}(${color2})(X)`,
      `${color}(${color2}${value})`,
      `${color}(${color2}X)`,
      // with a known number different from the real one
      `${color}${value}(${value2})`,
      `X${value}(${value2})`,
      `${color}${value}(${value2})`,
      `(${color})${value}(${value2})`,
      `${color}${value}(${value2})`,
      `X${value}(${value2})`,
      // with both known, number and value, different from the real ones
      `${color}(${color2})${value}(${value2})`,
      `X(${color2})${value}(${value2})`,
      `${color}(${color2})${value}(${value2})`,
      `${color}(${color2})${value}(${value2})`,
      `${color}${value}(${color2}${value2})`,
      `X${value}(${color2}${value2})`,
    ];
    const expected = [
      {
        color: { known: color, real: color },
        value: { known: value, real: value },
        clued: false,
        title: given[0],
      },
      {
        color: { known: color, real: color },
        value: { known: null, real: null },
        clued: false,
        title: given[1],
      },
      {
        color: { known: null, real: null },
        value: { known: value, real: value },
        clued: false,
        title: given[2],
      },

      {
        color: { known: color, real: color },
        value: { known: null, real: value },
        clued: false,
        title: given[3],
      },
      {
        color: { known: null, real: null },
        value: { known: null, real: value },
        clued: false,
        title: given[4],
      },
      {
        color: { known: null, real: color },
        value: { known: value, real: value },
        clued: false,
        title: given[5],
      },

      {
        color: { known: null, real: color },
        value: { known: null, real: null },
        clued: false,
        title: given[6],
      },
      {
        color: { known: null, real: color },
        value: { known: null, real: value },
        clued: false,
        title: given[7],
      },
      {
        color: { known: null, real: color },
        value: { known: null, real: null },
        clued: false,
        title: given[8],
      },
      {
        color: { known: null, real: null },
        value: { known: null, real: value },
        clued: false,
        title: given[9],
      },
      {
        color: { known: null, real: color },
        value: { known: null, real: value },
        clued: false,
        title: given[10],
      },
      {
        color: { known: null, real: color },
        value: { known: null, real: null },
        clued: false,
        title: given[11],
      },
      {
        color: { known: null, real: null },
        value: { known: null, real: value },
        clued: false,
        title: given[12],
      },
      {
        color: { known: null, real: null },
        value: { known: null, real: null },
        clued: false,
        title: given[13],
      },
      {
        color: { known: null, real: null },
        value: { known: null, real: null },
        clued: false,
        title: given[14],
      },
      {
        color: { known: null, real: null },
        value: { known: null, real: null },
        clued: false,
        title: given[15],
      },
      {
        color: { known: null, real: null },
        value: { known: null, real: null },
        clued: false,
        title: given[16],
      },
      {
        color: { known: null, real: null },
        value: { known: null, real: null },
        clued: false,
        title: given[17],
      },
      {
        color: { known: color, real: color2 },
        value: { known: value, real: value },
        clued: false,
        title: given[18],
      },
      {
        color: { known: color, real: color2 },
        value: { known: null, real: null },
        clued: false,
        title: given[19],
      },
      {
        color: { known: color, real: color2 },
        value: { known: null, real: value },
        clued: false,
        title: given[20],
      },
      {
        color: { known: color, real: color2 },
        value: { known: null, real: null },
        clued: false,
        title: given[21],
      },
      {
        color: { known: color, real: color2 },
        value: { known: null, real: value },
        clued: false,
        title: given[22],
      },
      {
        color: { known: color, real: color2 },
        value: { known: null, real: null },
        clued: false,
        title: given[23],
      },
      {
        color: { known: color, real: color },
        value: { known: value, real: value2 },
        clued: false,
        title: given[24],
      },
      {
        color: { known: null, real: null },
        value: { known: value, real: value2 },
        clued: false,
        title: given[25],
      },
      {
        color: { known: color, real: color },
        value: { known: value, real: value2 },
        clued: false,
        title: given[26],
      },
      {
        color: { known: null, real: color },
        value: { known: value, real: value2 },
        clued: false,
        title: given[27],
      },
      {
        color: { known: color, real: color },
        value: { known: value, real: value2 },
        clued: false,
        title: given[28],
      },
      {
        color: { known: null, real: null },
        value: { known: value, real: value2 },
        clued: false,
        title: given[29],
      },

      {
        color: { known: color, real: color2 },
        value: { known: value, real: value2 },
        clued: false,
        title: given[30],
      },
      {
        color: { known: null, real: color2 },
        value: { known: value, real: value2 },
        clued: false,
        title: given[31],
      },
      {
        color: { known: color, real: color2 },
        value: { known: value, real: value2 },
        clued: false,
        title: given[32],
      },
      {
        color: { known: color, real: color2 },
        value: { known: value, real: value2 },
        clued: false,
        title: given[33],
      },
      {
        color: { known: color, real: color2 },
        value: { known: value, real: value2 },
        clued: false,
        title: given[34],
      },
      {
        color: { known: null, real: color2 },
        value: { known: value, real: value2 },
        clued: false,
        title: given[35],
      },
    ];
    given.forEach((item, index) => {
      test(`${index}. "${item}" card`, () => {
        expect(parse(item)).toMatchObject(expected[index]);
      });
    });

    const givenWithClue = given.map(item => '!' + item);
    const expectedWithClue = expected.map((item, index) => ({
      ...item,
      clued: true,
      clued1: true,
      clued2: false,
      title: givenWithClue[index],
    }));
    givenWithClue.forEach((item, index) => {
      test(`${index}. "${item}" card`, () => {
        expect(parse(item)).toMatchObject(expectedWithClue[index]);
      });
    });

    const givenWithSecondClue = given.map(item => '?' + item);
    const expectedWithSecondClue = expected.map((item, index) => ({
      ...item,
      clued: true,
      clued1: false,
      clued2: true,
      title: givenWithSecondClue[index],
    }));
    givenWithSecondClue.forEach((item, index) => {
      test(`${index}. "${item}" card`, () => {
        expect(parse(item)).toMatchObject(expectedWithSecondClue[index]);
      });
    });

    const givenWithBothClues = given.map(item => '?!' + item);
    const expectedWithBothClues = expected.map((item, index) => ({
      ...item,
      clued: true,
      clued1: true,
      clued2: true,
      title: givenWithBothClues[index],
    }));
    givenWithBothClues.forEach((item, index) => {
      test(`${index}. "${item}" card`, () => {
        expect(parse(item)).toMatchObject(expectedWithBothClues[index]);
      });
    });

    const givenWithReversedBothClues = given.map(item => '?!' + item);
    const expectedWithReversedBothClues = expected.map((item, index) => ({
      ...item,
      clued: true,
      clued1: true,
      clued2: true,
      title: givenWithReversedBothClues[index],
    }));
    givenWithReversedBothClues.forEach((item, index) => {
      test(`${index}. "${item}" card`, () => {
        expect(parse(item)).toMatchObject(expectedWithReversedBothClues[index]);
      });
    });
  });
});
