const { buildRegExpColorList, buildRegExpValueList } = require('../helper/regexp');
const { CONFIG } = require('../config');

const getParser = (availableColors) => (clueString) => {
  let cleanClueString = clueString.replace(' ', '');
  if (cleanClueString.length === 0) {
    return null;
  }

  const clueRegexp = getClueRegexp(availableColors);
  const groups = cleanClueString.match(clueRegexp);
  if (groups === null) {
    return null;
  }
  const color = groups[2];
  const value = groups[3];

  return {
    type: !!color ? 'color' : 'value',
    content: color || +value,
  };
};

exports.getParser = getParser;

const CLUE_REGEXP_TEMPLATE = '^((<REGEXP_PART_COLOR_LIST>)|(<REGEXP_PART_VALUE_LIST>))$';
const CLUE_REGEXP = CLUE_REGEXP_TEMPLATE
  .replace(/<REGEXP_PART_COLOR_LIST>/g, buildRegExpColorList(CONFIG.AVAILABLE_COLORS, false))
  .replace(/<REGEXP_PART_VALUE_LIST>/g, buildRegExpValueList(false));

const defaultClueRegexp = new RegExp(CLUE_REGEXP);

const getClueRegexp = (availableColors) => {
  return availableColors
    ? CLUE_REGEXP_TEMPLATE
      .replace(/<REGEXP_PART_COLOR_LIST>/g, buildRegExpColorList(availableColors, false))
      .replace(/<REGEXP_PART_VALUE_LIST>/g, buildRegExpValueList(false))
    : defaultClueRegexp;
};
