const { CONFIG } = require('../config');
const { getParser } = require('./clueParser');

const parse = getParser();

describe('parseClue()', () => {
  describe('Invalid', () => {
    test('Blank strings should be null clue', () => {
      expect(parse('')).toBe(null);
      expect(parse('    ')).toBe(null);
    });

    test('<InvalidString> should return null', () => {
      expect(parse('D')).toBe(null);
      expect(parse('X')).toBe(null);
      expect(parse('Invalid String')).toBe(null);
      expect(parse('0')).toBe(null);
      expect(parse('6')).toBe(null);
      expect(parse('99')).toBe(null);

      expect(parse('D 1')).toBe(null);
      expect(parse('X 1')).toBe(null);
      expect(parse('Invalid String B')).toBe(null);
      expect(parse('0 B')).toBe(null);
      expect(parse('6 Y')).toBe(null);
      expect(parse('99 Bk')).toBe(null);
    });

    test('<InvalidCaseInitial> should return null', () => {
      expect(parse(CONFIG.AVAILABLE_COLORS[0].toLowerCase())).toBe(null);
    });

    test('<InvalidColor> should return null', () => {
      expect(parse('X')).toBe(null);
      expect(parse('Z')).toBe(null);
      expect(parse('WRONG')).toBe(null);
    });

    test('<InvalidValue> should return null', () => {
      expect(parse('0')).toBe(null);
      expect(parse('6')).toBe(null);
      expect(parse('55')).toBe(null);
      expect(parse('99')).toBe(null);
    });
  });

  describe('Colors', () => {
    CONFIG.AVAILABLE_COLORS.forEach((item) => {
      test(`"${item}" clue`, () => {
        expect(parse(`${item}`)).toMatchObject({
          type: 'color',
          content: item,
        });
      });
    });
  });

  describe('Values', () => {
    CONFIG.AVAILABLE_VALUES.forEach((item) => {
      test(`"${item}" clue`, () => {
        expect(parse(`${item}`)).toMatchObject({
          type: 'value',
          content: item,
        });
      });
    });
  });
});
