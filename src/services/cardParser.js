const { buildRegExpColorList, buildRegExpValueList } = require('../helper/regexp');
const { CONFIG } = require('../config');

const getParser = (availableColors) => (cardString) => {
  let cleanCardString = cardString.trim();
  if (cleanCardString.length === 0) {
    return null;
  }

  const cardRegexp = getCardRegexp(availableColors);
  const groups = cleanCardString.match(cardRegexp);
  if (groups === null) {
    return null;
  }
  const clue = groups[1] || '';
  let knownColor = groups[5] || groups[13] || groups[15] || null;
  knownColor = knownColor !== 'X' ? knownColor : null;
  let realColor = groups[8] || groups[14] || null;
  realColor = realColor !== 'X' ? realColor : null;
  let knownValue = groups[6] || groups[18] || groups[20] || null;
  knownValue = knownValue !== null && knownValue !== 'X' ? +knownValue : null;
  let realValue = groups[10] || groups[19] || null;
  realValue = realValue !== null && realValue !== 'X' ? +realValue : null;

  return {
    color: {
      known: knownColor,
      real: realColor || knownColor,
    },
    value: {
      known: knownValue,
      real: realValue || knownValue,
    },
    clued: !!clue,
    clued1: clue.includes('!'),
    clued2: clue.includes('?'),
    title: cleanCardString,
  };
};

exports.getParser = getParser;

// ((<C>)?[(](<C>)[)])|(<C>)
const REGEXP_PART_CARD_COLOR = '((<REGEXP_PART_COLOR_LIST>)?[(](<REGEXP_PART_COLOR_LIST>)[)])|(<REGEXP_PART_COLOR_LIST>)';
// ((<V>)?[(](<V>)[)])|(<V>)
const REGEXP_PART_CARD_VALUE = '((<REGEXP_PART_VALUE_LIST>)?[(](<REGEXP_PART_VALUE_LIST>)[)])|(<REGEXP_PART_VALUE_LIST>)';
// ((<C>)(<V>)?)?([(](<C>))((<V>)[)])
const REGEXP_PART_CARD_COMBINED = '((<REGEXP_PART_COLOR_LIST>)(<REGEXP_PART_VALUE_LIST>)?)?([(](<REGEXP_PART_COLOR_LIST>))((<REGEXP_PART_VALUE_LIST>)[)])';
// ([!]?)(( ((<C>)(<V>)?)?([(](<C>))((<V>)[)]) )|( ((<C>)?[(](<C>)[)])|(<C>) )( ((<V>)?[(](<V>)[)])|(<V>) ))
const CARD_REGEXP_TEMPLATE = `([!?]*)((${REGEXP_PART_CARD_COMBINED})|(${REGEXP_PART_CARD_COLOR})(${REGEXP_PART_CARD_VALUE}))`;

const CARD_REGEXP = CARD_REGEXP_TEMPLATE
  .replace(/<REGEXP_PART_COLOR_LIST>/g, buildRegExpColorList(CONFIG.AVAILABLE_COLORS))
  .replace(/<REGEXP_PART_VALUE_LIST>/g, buildRegExpValueList());

const defaultCardRegexp = new RegExp(CARD_REGEXP);

const getCardRegexp = (availableColors) => {
  return availableColors
    ? CARD_REGEXP_TEMPLATE
      .replace(/<REGEXP_PART_COLOR_LIST>/g, buildRegExpColorList(availableColors))
      .replace(/<REGEXP_PART_VALUE_LIST>/g, buildRegExpValueList())
    : defaultCardRegexp;
};
