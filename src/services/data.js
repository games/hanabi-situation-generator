const { getParser: getCardParser } = require('./cardParser');
const { getParser: getClueParser } = require('./clueParser');
const { CONFIG } = require('../config');

const SAMPLE_DATA = {
  playersHand: [
    ['(X)1', 'XX', 'GX', 'X(5)'],
    ['Bk2', 'B(1)', '(B)X', '(B)(2)'],
    ['!(Y5)', 'B(1)', 'B(M)(1)', '(B)(2)', '<- Y'],
    ['!Bk2', '!R(M)2', '(B)3', 'W(M5)', '<- 2'],
    ['!(Y5)', '?R(M)2', '?B(M)(X)', '!?W(M5)', '<- 5,G'],
  ],

  highestCardsPlayed: [
    'BX', 'R1', 'G2', 'Y3', 'W4', 'M5', 'Bk2',
  ],
  discardedCards: [
    'B2', 'B2', 'B4', 'B3', 'G2', 'G3', 'R5', 'Y4', 'W1', 'W1', 'M1', 'Bk4',
  ],
  nbRemainingCardsInDeck: 36,
};

const getSituation = (
  playersHand,
  highestCardsPlayed,
  discardedCards,
  playersName,
  nbRemainingCardsInDeck,
) => {
  return {
    ...(playersHand ? { players: getPlayers(playersHand, playersName) } : {}),
    ...(highestCardsPlayed ? { highestCardsPlayed: getPlayed(highestCardsPlayed || []) } : {}),
    ...(discardedCards ? { discardedCards: getDiscarded(discardedCards || []) } : {}),
    ...(nbRemainingCardsInDeck ? { nbRemainingCardsInDeck } : {}),
  };
};

exports.getSituation = getSituation;
exports.SAMPLE_DATA = SAMPLE_DATA;

const getPlayed = (cards) => {
  return cards
    .map(getCardParser())
    .filter((card) => card !== null)
    .map(({ color, value }) => ({ color: color ? color.real : null, value: value ? value.real : null }))
    .filter(({ color }) => color !== null);
};

const getDiscarded = (cards) => {
  return cards
    .map(getCardParser())
    .filter((card) => card !== null)
    .map(({ color, value }) => ({ color: color ? color.real : null, value: value ? value.real : null }))
    .sort(cardSorter)
    .filter(({ color, value }) => color && value)
    .reduce((discarded, { color, value }) => {
      return discarded.set(
        color,
        [...(discarded.get(color) || []), value],
      );
    }, new Map());
};

/**
 * @param playersHand List of card/clue strings to parse, for each player
 * @param playersName List of the name of the players
 * @returns {object}
 */
const getPlayers = (playersHand, playersName) => {
  return playersHand
    .map((playerHand, index) => {
      const playerName = playersName.length > index ? playersName[index] : CONFIG.PLAYER_NAMES[index];

      const lastItemOfHand = (playerHand[playerHand.length - 1] || '').trim();
      const isHandClued = lastItemOfHand.startsWith('<-');

      return {
        name: playerName,
        hand: getCards(!isHandClued ? playerHand : playerHand.slice(0, playerHand.length - 2)),
        clue: getClue(!isHandClued ? playerHand : playerHand.slice(0, playerHand.length - 2)),
        clues: isHandClued ? getClues(lastItemOfHand.substring(2)) : [],
      };
    });
};

const cardSorter = (card1, card2) => {
  const colorComparison = card1.color.localeCompare(card2.color);
  if (colorComparison !== 0) {
    return colorComparison;
  }

  return card1.value < card2.value ? -1 : 1;
};

const getCards = (playerHand) => {
  return playerHand
    .map(getCardParser())
    .filter((card) => card !== null);
};

const getClue = (cardString) => {
  return cardString
    .map(getClueParser())
    .filter((clue) => clue !== null)
    [0] || { type: null, content: null };
};

const getClues = (clueString) => {
  return clueString
    .trim()
    .split(',')
    .map((clue) => clue.trim())
    .map(getClueParser())
    .filter((clue) => clue !== null)
    .slice(0, 2);
};
