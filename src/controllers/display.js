const { SAMPLE_DATA, getSituation } = require('../services/data');
const { getQueryParam, encodeToMultipleStringLists, parsePlayerHandStrings, parseStringList } = require(
  '../helper/request');

const handler = (req, res) => {
  const playersHand = getQueryParam(req, 'playersHand', encodeToMultipleStringLists(SAMPLE_DATA.playersHand));
  const highestCardsPlayed = getQueryParam(req, 'highestCardsPlayed', '');
  const discardedCards = getQueryParam(req, 'discardedCards', '');
  const playersName = getQueryParam(req, 'playersName', '');
  const nbRemainingCardsInDeck = getQueryParam(req, 'nbRemainingCardsInDeck');

  return res.render('pages/display', {
    gameSituation: getSituation(
      parsePlayerHandStrings(playersHand),
      parseStringList(highestCardsPlayed),
      parseStringList(discardedCards),
      parseStringList(playersName),
      nbRemainingCardsInDeck,
    ),
  });
};

exports.handler = handler;
