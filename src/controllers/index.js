const { CONFIG } = require('../config');
const { SAMPLE_DATA } = require('../services/data');
const {
  buildQueryString,
  encodeToMultipleStringLists,
  getBaseUrlFromRequest,
  getQueryParam,
  parsePlayerHandStrings,
  parseStringList, encodeToStringList,
} = require('../helper/request');

const handler = async (req, res) => {
  const playersHand = getQueryParam(req, 'playersHand') || encodeToMultipleStringLists(SAMPLE_DATA.playersHand);
  const highestCardsPlayed = getQueryParam(req, 'highestCardsPlayed');
  const discardedCards = getQueryParam(req, 'discardedCards');
  const playersName = getQueryParam(req, 'playersName');
  const nbRemainingCardsInDeck = getQueryParam(req, 'nbRemainingCardsInDeck');

  const queryParams = [
    ...(playersHand ? [['playersHand', playersHand]] : []),
    ...(highestCardsPlayed ? [['highestCardsPlayed', highestCardsPlayed]] : []),
    ...(discardedCards ? [['discardedCards', discardedCards]] : []),
    ...(playersName ? [['playersName', playersName]] : []),
    ...(nbRemainingCardsInDeck ? [['nbRemainingCardsInDeck', nbRemainingCardsInDeck]] : []),
  ];

  return res.render('pages/index', {
    currentUrl: `${getBaseUrlFromRequest(req)}/`,
    iframeSrc: `${getBaseUrlFromRequest(req)}/display/${buildQueryString(queryParams)}`,
    situation: {
      playersHand: parsePlayerHandStrings(playersHand || ''),
      highestCardsPlayed: parseStringList(highestCardsPlayed || ''),
      discardedCards: parseStringList(discardedCards || ''),
      playersName: parseStringList(playersName || ''),
      nbRemainingCardsInDeck: +nbRemainingCardsInDeck,
    },
    sampleSituation: {
      playersHand: parsePlayerHandStrings(playersHand || ''),
      highestCardsPlayed: encodeToStringList(SAMPLE_DATA.highestCardsPlayed),
      discardedCards: encodeToStringList(SAMPLE_DATA.discardedCards),
      playersName: encodeToStringList(CONFIG.PLAYER_NAMES),
      nbRemainingCardsInDeck: SAMPLE_DATA.nbRemainingCardsInDeck,
    },
  });
};

exports.handler = handler;
