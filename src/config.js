const CONFIG = {
  // Bk need to be before B, or will be read a B
  AVAILABLE_COLORS: ['Bk','BK','K','B', 'R', 'G', 'Y', 'W', 'M', 'P', 'T', 'Pk', 'PK', 'Bn', 'BN'],
  AVAILABLE_VALUES: [1, 2, 3, 4, 5],
  PLAYER_NAMES: ['Alice', 'Bob', 'Carole', 'David', 'Eve'],
}

exports.CONFIG = CONFIG;
