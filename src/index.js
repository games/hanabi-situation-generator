const PORT = process.env.PORT || 4200;

const express = require('express');
const path = require('path');

const app = express();

// Template engine
app.use(express.static(path.join(__dirname, 'public')))
  .set('views', path.join(__dirname, 'views'))
  .set('view engine', 'ejs');

const pages = [
  { title: 'Configure', path: '/', handler : require('./controllers').handler},
  { title: 'Display', path: '/display', handler : require('./controllers/display').handler},
]
for (const {path, handler} of pages) {
  app.get(path, handler);
}

app.listen(PORT, () => {
  console.log(`Server launched, listening on port ${PORT}: http://localhost:4200`);
  console.log('\nPages available :');
  for (const {path, title} of pages) {
    console.log(`  - ${path}: ${title}`)
  }
});
