.DEFAULT_GOAL := help
help:
	@grep -E '(^[1-9a-zA-Z_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}{printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'
.PHONY: help

## -------------
## Images
## -------------

build: ## Builds the docker image
	./docker/build.sh "games/hanabi-situation-generator"


## -------------
## Tools
## -------------

install: ## Installs required dependencies
	docker-compose -p application -f ./docker/docker-compose.yml run --rm application \
	npm install

start: ## Runs the application
	docker-compose -p application -f ./docker/docker-compose.yml run --rm -p 4200:4200 application \
	npm start

test: ## Tests the application
	docker-compose -p application -f ./docker/docker-compose.yml run --rm application \
	npm test

bash: ## Starts a shell session
	docker-compose -p application -f ./docker/docker-compose.yml run --rm application \
	/bin/sh

package: ## Packages the prod version of the application
	docker-compose -p application -f ./docker/docker-compose-package.yml run --rm application rm -Rf node_modules
	docker-compose -p application -f ./docker/docker-compose-package.yml run --rm application \
	npm install --omit=dev
	mkdir -p ci_build
	docker run -i --rm -v $(shell pwd):/srv/sources:rw registry.gitlab.com/v.nivuahc/docker-tools/package:latest

