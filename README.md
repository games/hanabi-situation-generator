Hanabi Situation Generator
==========================

A situation is a representation of a moment during a Hanabi's game.

The generator can display several elements:

- the players' hand
- clues on it
- the cards played
- the cards discarded
- the number of remaining cards in the deck

The application is online here: <https://hanabi.devnotebook.fr>

![UI preview](./doc/generator.jpg)

## Format

### Card format

Here's a list of accepted card formats:

```
// A card can be
// known
Bk1
YX
X2
// partially known
B(1)
X(1)
(B)3
(B)X
(B)(4)
(B)(X)
(X)(5)
(Y5)
(YX)
(X5)
// unknown
XX
X(X)
(X)X
(X)(X)
(XX)
// with a known color different from the real one
Y(M)2
B(M)X
W(M)(1)
W(M)(X)
R(M4)
R(MX)
// with a known number different from the real one
Y2(3)
X1(2)
W5(1)
(W)4(3)
G5(4)
X5(3)
// with both known, number and value, different from the real ones
Y(M)2(3)
X(M)1(2)
W(M)5(1)
W(M)4(3)
G5(M4)
X5(M3)

// All the previous cards can be prefixed by a ! to make to make them marked by the clue
```

### Colors

Here is the list of the handled colors: 

- `B`: **B**lue
- `R`: **R**ed
- `Y`: **Y**elllow
- `G`: **G**reen
- `W`: **W**hite
- `M`: **M**ulticolor
- `Bk` (and `Bk` and `K`): **B**lac**k**
- `P`: **P**urple
- `Teal`: **T**eal
- `Pk` (and `PK`): **P**in**k**
- `Bn` (and `BN`): **B**row**n**
- `X`: any color (use this when the color does not matter)


## Dev

### Requirements

To easily run the application locally, you need:

- Docker (+ docker-compose)
- The **make** command be available

### Installation

To build the docker image and install the application dependencies, use these commands:

```bash
make build
make install
```

### Start

To start the application, use this command:

```bash
make start
```

### Access bash

To access to a bash session into the docker container, use this command:

```bash
make bash
```

### Refresh

The application does not handle hot refreshing. The container must be restarted after each change.  
Use `Ctrl+C` to stop the container, and `make start` again to restart it.

### Build

To build the production version, which can be deployed to a server, use this command:

```bash
make package
```

The application is now available in the `ci_build/hanabi-situation-generator.zip` archive.

**Note** : The command **remove the dev dependencies** to only keep the production ones. To get them back, just use 
the installation command again:

```bash
make install
```
